package xyz.znix.jmhplayground;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Fork(1)
@Warmup(iterations = 2, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@SuppressWarnings({"StringOperationCanBeSimplified", "StringEquality"})
@BenchmarkMode(Mode.AverageTime)
public class StringCompareTest {

    @State(Scope.Thread)
    public static class MyState {
        @Param({"1", "5", "10", "15", "1000"})
        int length;

        @Setup(Level.Trial)
        public void doSetup() {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
                sb.append((char) ('A' + (i % 51)));

            // Do it in this exact order to avoid these being equal unintentionally
            interned = sb.toString().intern();
            nonInterned = new String(interned);
            nonInternedDifferentLength = nonInterned + "!";

            if (interned == nonInterned)
                throw new IllegalStateException("Strings equal!");
        }

        public String interned;
        public String nonInterned;
        public String nonInternedDifferentLength;
    }

    @Benchmark
    public boolean timeCompare(MyState s) {
        return s.interned == s.nonInterned;
    }

    @Benchmark
    public boolean timeEqualsCross(MyState s) {
        return s.interned.equals(s.nonInterned);
    }

    @Benchmark
    public boolean timeEqualsInterned(MyState s) {
        //noinspection EqualsWithItself
        return s.interned.equals(s.interned);
    }

    @Benchmark
    public boolean timeEqualsDifferentLength(MyState s) {
        return s.nonInterned.equals(s.nonInternedDifferentLength);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(StringCompareTest.class.getSimpleName())
                .param("length", "42")
                .build();

        new Runner(opt).run();
    }
}
